#!/usr/bin/python3
# -*- coding:utf-8 -*-

import numpy
import sys
import matplotlib.pyplot as plt


def load_csv(filename):
    """ 从CSV中载入数据

    :param filename: 文件名
    :return: X, Y, W(权重)
    """
    tmp = numpy.loadtxt(filename, delimiter=",", skiprows=1)
    X = tmp[:, 0]
    Y = tmp[:, 1]
    if tmp.shape[1] == 3:
        W = tmp[:, 2]
    else:
        W = numpy.ones(X.shape)
    return X, Y, W


def phi_G(k, l, X, W):
    """ 计算 Gram 矩阵的一项

    :param k: phi(k)
    :param l: phi(l)
    :param X: X
    :param W: 权重
    :return: (phi(k), phi(l))
    """
    tmp = numpy.power(X, k + l) * W
    return numpy.sum(tmp)


def phi_d(l, X, Y, W):
    """ 计算 d 中的一项

    :param l: phi(l)
    :param X: X
    :param Y: Y
    :param W: W
    :return: (Y, phi(l))
    """
    tmp = numpy.power(X, l) * Y * W
    return numpy.sum(tmp)


if __name__ == "__main__":
    # Get param from argv
    if len(sys.argv) <= 2:
        order = 3 + 1
    else:
        order = sys.argv[2] + 1
    if len(sys.argv) == 1:
        filename = "data43.csv"
    else:
        filename = sys.argv[1]
    # Load data
    X, Y, W = load_csv(filename)
    # Calculate Gram Matrix G
    G = numpy.zeros((order, order))
    for i in range(order):
        for j in range(order):
            G[i, j] = phi_G(i, j, X, W)
    # Calculate d
    d = numpy.zeros(order)
    for i in range(order):
        d[i] = phi_d(i, X, Y, W)
    print('G =\n', G)
    print("d' =", d)
    # Solve G * alpha = d
    alpha = numpy.linalg.solve(G, d)
    print("α' =", alpha)
    # Calculate theta^2
    Yb = numpy.zeros(X.shape)
    for i in range(X.shape[0]):
        for k in range(order):
            Yb[i] += (alpha[k] * numpy.power(X[i], k))
    print('δ² =', numpy.sum(numpy.power(Y - Yb, 2)))
    # Plot real value
    fig = plt.figure().add_subplot("111")
    fig.plot(X, Y, color='b', linestyle='', marker='*')
    # Plot fitting result
    Xplot = numpy.arange(min(X) - (max(X) - min(X)) / 10,
                         max(X) + (max(X) - min(X)) / 10,
                         (max(X) - min(X)) / 40)
    Yplot = numpy.zeros(Xplot.shape)
    for i in range(Xplot.shape[0]):
        for k in range(order):
            Yplot[i] += (alpha[k] * numpy.power(Xplot[i], k))

    fig.plot(Xplot, Yplot, color='g', linestyle='-', marker='')
    # Set title, label, etc...
    fig.set_title("Least Squares Fitting")
    fig.set_xlabel("X")
    fig.set_ylabel("Y")
    fig.legend(["Measurements", "Predictive"])
    plt.show()
