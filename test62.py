#!/usr/bin/python3
# -*- coding:utf-8 -*-

from scipy.integrate import solve_ivp, OdeSolver
import matplotlib.pyplot as plt
import numpy


class ODE1:
    """
    题目一
    """

    @staticmethod
    def dy(t, y):
        return -y + 2 * numpy.cos(t)

    y_init = numpy.array([1])
    t_span = [0., numpy.pi]

    @staticmethod
    def y(t):
        return numpy.cos(t) + numpy.sin(t)


class ODE2:
    """
    题目二
    """

    @staticmethod
    def dy(t, y):
        return (2. / t - 1.) * y - 1. * numpy.square(t) * numpy.exp(1)

    y_init = numpy.array([4 * (numpy.exp(-2) - numpy.e)])
    t_span = [2., 3.]

    @staticmethod
    def y(t):
        return numpy.power(t, 2) * (numpy.power(numpy.e, -t) - numpy.e)


class OdeSingleStepSolver(OdeSolver):
    """
    单步求解器
    """

    def __init__(self, fun, t0, y0, t_bound, vectorized, step=None):
        super().__init__(fun, t0, y0, t_bound, vectorized)
        if step is None:
            self.h_abs = numpy.abs(t0 - t_bound) / 20
        else:
            self.h_abs = step

    def _step_impl(self):
        t = self.t
        y = self.y

        h = self.h_abs * self.direction
        t_new = t + h

        if self.direction * (t_new - self.t_bound) > 0:
            t_new = self.t_bound

        h = t_new - t

        y_new = y + self._dy(t, y, h)

        self.y_old = y

        self.t = t_new
        self.y = y_new

        return True, None

    def _dense_output_impl(self):
        return NotImplementedError

    def _dy(self, t, y, h):
        return h * self.fun(t, y)


class RK3C(OdeSingleStepSolver):
    """
    显式 3阶 Runge-Kutta 公式
    """

    def _dy(self, t, y, h):
        f = self.fun
        k1 = f(t, y)
        k2 = f(t + 0.5 * h, y + 0.5 * h * k1)
        k3 = f(t + h, y - k1 * h + 2 * k2 * h)
        return h / 6. * (k1 + 4 * k2 + k3)


class RK4C(OdeSingleStepSolver):
    """
    显式 4阶 Runge-Kutta 公式
    """

    def _dy(self, t, y, h):
        k1 = h * self.fun(t, y)
        k2 = h * self.fun(t + 0.5 * h, y + 0.5 * k1)
        k3 = h * self.fun(t + 0.5 * h, y + 0.5 * k2)
        k4 = h * self.fun(t + h, y + k3)
        return (k1 + k2 + k2 + k3 + k3 + k4) / 6


def solveC(model, solver, csv, fig, legend, plot_mark):
    t_ = numpy.arange(model.t_span[0],
                      model.t_span[1] + (model.t_span[1] - model.t_span[0]) / 10.,
                      (model.t_span[1] - model.t_span[0]) / 10.)
    result = solve_ivp(model.dy, model.t_span, model.y_init,
                       method=solver, step=(model.t_span[1] - model.t_span[0]) / 10.)
    fig.plot(t_, result.y[0], plot_mark)
    mse = numpy.average(numpy.power(result.y[0] - model.y(result.t), 2))
    legend.append("%s MSE=%.2e" % (solver.__name__, mse))
    csv.append(result.y[0])


def solveI(model, solver, csv, fig, legend, plot_mark):
    t_ = numpy.arange(model.t_span[0],
                      model.t_span[1] + (model.t_span[1] - model.t_span[0]) / 10.,
                      (model.t_span[1] - model.t_span[0]) / 10.)
    result = solve_ivp(model.dy, model.t_span, model.y_init,
                       method=solver, dense_output=True)
    fig.plot(t_, result.sol(t_)[0], plot_mark)
    mse = numpy.average(numpy.power(result.sol(t_)[0] - model.y(t_), 2))
    legend.append("%s MSE=%.2e" % (solver if solver.__class__ == str else solver.__name__, mse))
    csv.append(result.sol(t_)[0])


def main():
    ODEs = [ODE1, ODE2]
    # ODEs = [ODE2]
    for ODE in ODEs:
        # 初始化绘图模块
        fig = plt.figure().add_subplot("111")
        legend = []
        # 绘制函数真值
        t_ = numpy.arange(ODE.t_span[0],
                          ODE.t_span[1] + (ODE.t_span[1] - ODE.t_span[0]) / 40.,
                          (ODE.t_span[1] - ODE.t_span[0]) / 40.)
        fig.plot(t_, ODE.y(t_), '-.')
        legend.append("y(t)")
        # 设置10个等距时间点
        t_ = numpy.arange(ODE.t_span[0],
                          ODE.t_span[1] + (ODE.t_span[1] - ODE.t_span[0]) / 10.,
                          (ODE.t_span[1] - ODE.t_span[0]) / 10.)
        # CSV写入缓存
        csv = [t_, ODE.y(t_)]
        # 显式 Runge-Kutta (2,3) 公式 (对应 MATLAB ode23)
        solveI(ODE, "RK23", csv, fig, legend, '1')
        # 显式 Runge-Kutta (4,5) 公式 (对应 MATLAB ode45)
        solveI(ODE, "RK45", csv, fig, legend, '2')
        # 3 阶经典龙格库塔法
        solveC(ODE, RK3C, csv, fig, legend, plot_mark='3')
        # 4 阶经典龙格库塔法
        solveC(ODE, RK4C, csv, fig, legend, plot_mark='4')
        # 显示图像
        fig.legend(legend)
        fig.set_title("%s\n(ode23 vs. ode45 vs. classical 3rd/4th-order RK)" % ODE.__name__)
        fig.set_xlabel("t")
        fig.set_ylabel("y(t)")
        plt.show()
        # 保存CSV
        csv = numpy.array(csv).transpose()
        header = "t"
        for l in legend:
            header += ',' + l
        numpy.savetxt("data62_%s.csv" % ODE.__name__, csv, delimiter=',', header=header)

        # 外推
        fig = plt.figure().add_subplot("111")
        legend = []
        result_4c = solve_ivp(ODE.dy, ODE.t_span, ODE.y_init,
                              method=RK4C, step=(ODE.t_span[1] - ODE.t_span[0]) / 10.)
        result_4c2h = solve_ivp(ODE.dy, ODE.t_span, ODE.y_init,
                                method=RK4C, step=(ODE.t_span[1] - ODE.t_span[0]) / 10. * 2)
        result_ex = (32 * result_4c.y[0][::2] - result_4c2h.y[0]) / 31

        t_ = numpy.arange(ODE.t_span[0],
                          ODE.t_span[1] + (ODE.t_span[1] - ODE.t_span[0]) / 40.,
                          (ODE.t_span[1] - ODE.t_span[0]) / 40.)
        fig.plot(t_, ODE.y(t_), '-.')
        legend.append("y(t)")
        fig.plot(result_4c.t, result_4c.y[0], '1')
        mse = numpy.average(numpy.power(result_4c.y[0] - ODE.y(result_4c.t), 2))
        legend.append("%s MSE=%.2e" % ("RK4C", mse))
        fig.plot(result_4c2h.t, result_4c2h.y[0], '2')
        mse = numpy.average(numpy.power(result_4c2h.y[0] - ODE.y(result_4c2h.t), 2))
        legend.append("%s MSE=%.2e" % ("RK4C_2h", mse))
        fig.plot(result_4c2h.t, result_ex, '+')
        mse = numpy.average(numpy.power(result_ex - ODE.y(result_4c2h.t), 2))
        legend.append("%s MSE=%.2e" % ("RK4C_ex", mse))
        fig.legend(legend)
        fig.set_title("%s\n(classical 4th-order RK vs. Extrapolation)" % ODE.__name__)
        fig.set_xlabel("t")
        fig.set_ylabel("y(t)")
        plt.show()


if __name__ == "__main__":
    main()
